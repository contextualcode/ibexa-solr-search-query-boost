<?php

declare(strict_types=1);

namespace ContextualCode\IbexaSolrSearchQueryBoost\Query\CriterionVisitor\FullText;

use ContextualCode\IbexaSolrSearchQueryBoost\Query\Criterion\FullText as FullTextCriterion;
use Ibexa\Contracts\Core\Repository\Values\Content\Query\Criterion;
use Ibexa\Solr\Query\Content\CriterionVisitor\FullText as Base;
use Ibexa\Contracts\Solr\Query\CriterionVisitor;
use Ibexa\Core\Search\Common\FieldNameResolver;
use QueryTranslator\Languages\Galach\Generators\ExtendedDisMax;
use QueryTranslator\Languages\Galach\Parser;
use QueryTranslator\Languages\Galach\Tokenizer;

class FullText extends Base
{
    /**
     * @var BoostQueryParser
     */
    protected $boostQueryParser;

    public function __construct(
        FieldNameResolver $fieldNameResolver,
        Tokenizer $tokenizer,
        Parser $parser,
        ExtendedDisMax $generator,
        BoostQueryParser $boostQueryParser,
        $maxDepth = 0
    ) {
        parent::__construct(
            $fieldNameResolver,
            $tokenizer,
            $parser,
            $generator,
            $maxDepth
        );

        $this->boostQueryParser = $boostQueryParser;
    }

    public function canVisit(Criterion $criterion)
    {
        return $criterion instanceof FullTextCriterion;
    }

    /**
     * Map field value to a proper Solr representation.
     *
     * @param Criterion $criterion
     * @param CriterionVisitor|null $subVisitor
     *
     * @return string
     */
    public function visit(Criterion $criterion, CriterionVisitor $subVisitor = null)
    {
        /** @var FullTextCriterion $criterion */
        $tokenSequence = $this->tokenizer->tokenize($criterion->value);
        $syntaxTree = $this->parser->parse($tokenSequence);

        $options = [];
        if ($criterion->fuzziness < 1) {
            $options['fuzziness'] = $criterion->fuzziness;
        }

        $queryString = $this->generator->generate($syntaxTree, $options);
        $queryStringEscaped = $this->escapeQuote($queryString);
        $queryFields = $this->getQueryFields($criterion);
        $boostQuery = $this->boostQueryParser->parse($criterion);

        return "{!edismax v='{$queryStringEscaped}' bq='{$boostQuery}' qf='{$queryFields}' uf=-*}";
    }

    private function getQueryFields(Criterion $criterion)
    {
        /** @var FullTextCriterion $criterion */
        $queryFields = ['meta_content__text_t'];

        for ($i = 1; $i <= $this->maxDepth; ++$i) {
            $queryFields[] = "meta_related_content_{$i}__text_t^{$this->getBoostFactorForRelatedContent($i)}";
        }

        foreach ($criterion->boost as $field => $boost) {
            $searchFields = $this->getSearchFields($criterion, $field);

            foreach ($searchFields as $name => $fieldType) {
                $queryFields[] = $name . '^' . $boost;
            }
        }

        return implode(' ', $queryFields);
    }

    /**
     * Returns boost factor for the related content.
     */
    private function getBoostFactorForRelatedContent(int $depth): float
    {
        return 1.0 / pow(2.0, $depth);
    }
}