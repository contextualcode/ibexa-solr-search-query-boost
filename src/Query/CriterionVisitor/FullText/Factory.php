<?php

declare(strict_types=1);

namespace ContextualCode\IbexaSolrSearchQueryBoost\Query\CriterionVisitor\FullText;

use Ibexa\Core\Search\Common\FieldNameResolver;
use Ibexa\Solr\FieldMapper\IndexingDepthProvider;
use Ibexa\Solr\Query\Common\CriterionVisitor\Factory\FullTextFactoryAbstract;
use Ibexa\Contracts\Solr\Query\CriterionVisitor;
use QueryTranslator\Languages\Galach\Generators\ExtendedDisMax;
use QueryTranslator\Languages\Galach\Parser;
use QueryTranslator\Languages\Galach\Tokenizer;

/**
 * Factory for FullText Criterion Visitor.
 *
 * @see \Ibexa\Solr\Query\Content\CriterionVisitor\FullText
 *
 * @internal
 */
final class Factory extends FullTextFactoryAbstract
{
    /**
     * @var BoostQueryParser
     */
    private $boostQueryParser;

    public function __construct(
        FieldNameResolver $fieldNameResolver,
        Tokenizer $tokenizer,
        Parser $parser,
        ExtendedDisMax $generator,
        IndexingDepthProvider $indexingDepthProvider,
        BoostQueryParser $boostQueryParser
    ) {
        parent::__construct(
            $fieldNameResolver,
            $tokenizer,
            $parser,
            $generator,
            $indexingDepthProvider
        );

        $this->boostQueryParser = $boostQueryParser;
    }

    /**
     * Create FullText Criterion Visitor.
     *
     * @return CriterionVisitor|FullText
     */
    public function createCriterionVisitor(): CriterionVisitor
    {
        return new FullText(
            $this->fieldNameResolver,
            $this->tokenizer,
            $this->parser,
            $this->generator,
            $this->boostQueryParser,
            $this->indexingDepthProvider->getMaxDepth()
        );
    }
}
